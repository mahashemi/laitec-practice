$(document).ready(function() {

	$("a[href^='#']").on("click", function(event) {
		event.preventDefault();
	});

	$(".mainMenu .parentLi.dropdown").each(function(index, elem) {
		var anchor = $(this).find(".parentA");
		var list = $(this).find(".dropdown-content");
		$(anchor).on("click", function(event) {
			event.preventDefault();

			if ( $(list).is(".opened") ) {
				$(list).removeClass("opened").slideUp(200);
				$(anchor).removeClass("clicked");
			}
			else {
				$(".dropdown-content").removeClass("opened").slideUp(200);
				$(list).addClass("opened").slideDown(200);

				$(".parentA").removeClass("clicked");
				$(anchor).addClass("clicked");
			}
		});
	});

	$(".search button").on("click", function() {
		$(this).prev("input").toggleClass("opened");
	});

	$(".modal-opener").on("click", function(event) {
		event.preventDefault();
		$(".modal-content").fadeToggle(200);
		$(".mainNav").toggleClass("zIndex-1");
	});
	$(".modal-content .close").on("click", function() {
		$(this).closest(".modal-content").fadeOut(200);
		$(".mainNav").removeClass("zIndex-1");
	});


	var mainSlider = $("#mainSlider").slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		infinite: false,
		speed: 500,
		arrows: false,
		dots: true,
		fade: true,
		useTransform: false,
		autoplay: true,
  		autoplaySpeed: 5000,
	});
	$(".stopSlider").on("click", function() {
		$("#mainSlider").slick('slickPause');
	});

	$("#russianSlider").slick({
		slidesToShow: 3,
		centerMode: true,
		infinite: true,
		speed: 500,
		arrows: false,
		dots: false,
		autoplay: true,
		autoplaySpeed: 3000
	});

});
