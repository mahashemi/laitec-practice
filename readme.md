# Main stacks
- SASS
- Bower (for example: bower install jquery)


# Requirements
- An editor with Emmet plugin (http://emmet.io/)
- NodeJS installer (https://nodejs.org/en/download/)
	->	npm
		->	node-sass,
			bower ->
				JQuery,
				Susy,
				Vazir font,
				Slick carousel
- Genericons (https://github.com/Automattic/genericons-neue)
- Google Chrome!
	- Design Grid Overlay (https://chrome.google.com/webstore/detail/design-grid-overlay/kmaadknbpdklpcommafmcboghdlopmbi)


## Session 1
Converting An image to HTML and CSS

## Session 2
Making our View interactive

## Session 3
Making our View responsive
